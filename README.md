# Python Books - Frontend
[![Netlify Status](https://api.netlify.com/api/v1/badges/23e42853-d686-48fc-a46d-a694c3a447f0/deploy-status)](https://app.netlify.com/sites/affectionate-leavitt-2968e6/deploys)

Further information available at https://bitbucket.org/aijorgenson/pythonbooks/src/master/
## Project setup
```
yarn install
cp .env.example .env
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
