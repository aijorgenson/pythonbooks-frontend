import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    bearerToken: null,
    libraryBooks: [],
    bookSearchQuery: "",
    searchedBooks: [],
  },
  getters: {
    isAuthenticated: state => {
        return state.bearerToken !== null
    },
    libraryBooks: state => state.libraryBooks,
    bookSearchQuery: state => state.bookSearchQuery,
    searchedBooks: state => state.searchedBooks,
  },
  actions: {
    loadLibrary({state}) {
      axios.get('api/library').then((response) => {
        state.libraryBooks = response.data
      })
    },
    removeBookFromLibrary({commit}, bookId) {
      axios.post('api/book/remove', {book_id: bookId}).then(() => {
        commit('removeBook', bookId)
      })
    },
    addBookToLibrary({dispatch}, bookId) {
      axios.post('api/book/add', {book_id: bookId}).then(() => {
        dispatch('loadLibrary')
      })
    },
    searchBooks({state}) {
      axios.get('/api/book/search', {params: {query: state.bookSearchQuery}}).then((response) => {
        // no idea why axios doesn't like the JsonResponse from django
        state.searchedBooks = JSON.parse(response.data).items
      })
    }
  },
  mutations: {
    setBearerToken (state, bearerToken) {
      state.bearerToken = bearerToken
      localStorage.token = bearerToken
      axios.defaults.headers.common = {
        Authorization: `Bearer ${localStorage.token}`
      }
    },
    setBookSearchQuery (state, query) {
      state.bookSearchQuery = query
    },
    removeBook(state, bookId) {
      Vue.set(state, 'libraryBooks', state.libraryBooks.filter(book => book.pk != bookId))
    }
  }
})

export default store