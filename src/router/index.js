import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Library from '../views/Library.vue'
import Login from '../views/Login.vue'
import Search from '../views/Search.vue'
import oktaClient from '../oktaClient'

Vue.use(VueRouter)

const routes = [
  // private routes
  { path: '/library', component: Library, beforeEnter: requireAuth },
  { path: '/search', component: Search, beforeEnter: requireAuth },
  // public routes
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  { path: '/about', component: About },
  { path: '/login', component: Login },
  { path: '/logout',
    beforeEnter (to, from, next) {
      oktaClient.logout()
      next('/')
    }
  }
]

function requireAuth (to, from, next) {
  if (!oktaClient.loggedIn()) {
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
  } else {
    next()
  }
}


const router = new VueRouter({
  routes
})

export default router
