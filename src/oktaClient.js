const OktaAuth = require('@okta/okta-auth-js').OktaAuth
const authClient = new OktaAuth({issuer: process.env.VUE_APP_OKTA_AUTH_ISSUER})

export default {
  login (store, email, pass, cb) {
    cb = arguments[arguments.length - 1]
    if (localStorage.token) {
      if (cb) cb(true)
      this.onChange(true)
      return
    }
    return authClient.signIn({
      username: email,
      password: pass
    }).then(transaction => {
      if (transaction.status === 'SUCCESS') {
        return authClient.token.getWithoutPrompt({
          clientId: process.env.VUE_APP_OKTA_AUTH_CLIENT_ID,
          responseType: ['id_token', 'token'],
          scopes: ['openid', 'email', 'profile'],
          sessionToken: transaction.sessionToken,
          redirectUri: window.location.origin + '/login/callback'
        }).then(response => {
          localStorage.token = response.tokens.accessToken.value
          store.commit('setBearerToken', localStorage.token)
          localStorage.idToken = response.tokens.idToken.value
          if (cb) cb(true)
          this.onChange(true)
        })
      }
    }).catch(err => {
      console.error(err.message)
      if (cb) cb(false)
      this.onChange(false)
    })
  },

  getToken () {
    return localStorage.token
  },

  logout (cb) {
    delete localStorage.token
    delete localStorage.idToken
    if (cb) cb()
    this.onChange(false)
    return authClient.signOut()
  },

  loggedIn () {
    return !!localStorage.token
  },

  onChange () {
  }
}
