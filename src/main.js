import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/tailwind.css'
import axios from 'axios'
if (process.env.NODE_ENV !== 'development') {
  axios.defaults.baseURL = 'https://pythonbooks.api.andrewjorgenson.net'
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

if (localStorage.token) {
  store.commit('setBearerToken', localStorage.token)
}
